:title: hyprcursor-util

NAME
====
hyprcursor-util - A utility to compile, pack, unpack, etc. hyprcursor and xcursor themes

SYNOPSIS
========
**hyprcursor-util** -c <path> [-o <dir>]

**hyprcursor-util** -x <path> [-o <dir>] [--resize [mode]]

OPTIONS
=======
**-c <path>, --create <path>**
    Create a compiled hyprcursor theme from a working state

**-x <path>, --extract <path>**
    Extract an xcursor theme into a working state. This option uses the 
    external program **xcur2png(1)** for converting xcursor theme files to
    a format required by hyprcursor.

**-o <path-to-dir>, --output <path-to-dir>**
    Both **--create** and **--extract** supports the **-output** option to 
    specify an output directory. For safety reasons, **do not use this on 
    versions below 0.1.1** as it will nuke the specified directory without 
    asking.

    Since v0.1.2, this directory is the parent, the theme will be written to 
    a subdirectory in it called **ACTION_NAME**.

**--resize [mode]**
    For use with **--extract** option. Specify a default resize algorithm for
    shapes. Default is **none**.

**-v, --version**
    Print info about the version of hyprcursor-util.

DESCRIPTION
===========
Cursor themes can be in 3 states:
     * **compiled hyprcursor** - these can be used by apps / compositors
     * **compiled xcursor** - these can be used by xcursor
     * **working state** - an easy to navigate mode where every cursor is a png/svg,
       and all the meta is in files

AUTHORS
=======
This man page is written for the Debian GNU/Linux system by Alan M Varghese.
The information in this page is collected from the official README file
included with hyprcursor-util source files.
